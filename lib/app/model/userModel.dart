class UserData {
  String? id;
  String? nama;
  String? noTelp;
  String? foto;
  String? alamat;
  String? role;
  String? email;
  String? creationTime;
  String? gender;

  UserData(
      {this.id,
      this.nama,
      this.noTelp,
      this.foto,
      this.alamat,
      this.role,
      this.email,
      this.creationTime,
      this.gender});

  UserData.fromJson(Map<String, dynamic> json) {
    UserData(
        id: json['id'],
        nama: json['nama'],
        noTelp: json['noTelp'],
        foto: json['foto'],
        alamat: json['alamat'],
        role: json['role'],
        email: json['email'],
        creationTime: json['creationTime'],
        gender: json["gender"]);
  }
  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "nama": nama,
      "noTelp": noTelp,
      "foto": foto,
      "alamat": alamat,
      "role": role,
      "email": email,
      "creationTime": creationTime,
      "gender": gender
    };
  }
}
