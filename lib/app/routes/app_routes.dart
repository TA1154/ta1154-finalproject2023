part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const LOGIN_PAGE = _Paths.LOGIN_PAGE;
  static const HOME = _Paths.HOME;
  static const DASHBOARD = _Paths.DASHBOARD;
  static const DAFTAR = _Paths.DAFTAR;
  static const PROFIL = _Paths.PROFIL;
  static const LAPOR = _Paths.LAPOR;
  static const DETAIL_PROFIL = _Paths.DETAIL_PROFIL;
  static const GANTI_PASSWORD = _Paths.GANTI_PASSWORD;
}

abstract class _Paths {
  _Paths._();
  static const LOGIN_PAGE = '/login-page';
  static const HOME = '/home';
  static const DASHBOARD = '/dashboard';
  static const DAFTAR = '/daftar';
  static const PROFIL = '/profil';
  static const LAPOR = '/lapor';
  static const DETAIL_PROFIL = '/detail-profil';
  static const GANTI_PASSWORD = '/ganti-password';
}
