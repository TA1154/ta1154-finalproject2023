import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../model/userModel.dart';
import '../routes/app_pages.dart';

class AuthCController extends GetxController {
  FirebaseAuth auth = FirebaseAuth.instance;
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  TextEditingController emailC = TextEditingController();
  TextEditingController passwordC = TextEditingController();
  RxBool hidepass = true.obs;
  User? _currentUser;

  Stream<User?> get streamStatusAuth => auth.userChanges();

  @override
  void onInit() {
    super.onInit();
  }

  void lupaPass(String email) async {
    if (email != "" && GetUtils.isEmail(email)) {
      try {
        await auth.sendPasswordResetEmail(email: email);
        Get.defaultDialog(
            title: "Berhasil",
            middleText:
                "Berhasil megirimkan reset password ke $email , silahkan cek email anda",
            onConfirm: () {
              Get.back();
              Get.back();
            },
            textConfirm: "oke");
      } catch (e) {
        Get.defaultDialog(
          title: "terjadi kesalahan",
          middleText:
              "Tidak bisa mengirim verifikasi ulang, coba beberapa saat lagi",
        );
      }
    } else {
      Get.defaultDialog(
        title: "terjadi kesalahan",
        middleText: "Email belum terdaftar",
      );
    }
  }

  Future<void> login(String email, String password) async {
    if (email != "" && GetUtils.isEmail(email)) {
      try {
        UserCredential userlog = await auth.signInWithEmailAndPassword(
            email: email, password: password);

        // print(userlog.user);
        if (userlog.user!.emailVerified) {
          if (userlog != null) {
            _currentUser = FirebaseAuth.instance.currentUser;
            final userId = _currentUser?.email;
            print(userId);
            Get.offAllNamed(Routes.DASHBOARD);
            // print(userId);
            // await FirebaseFirestore.instance
            //     .collection("users")
            //     .doc(userId)
            //     .get()
            //     .then((value) {
            //   Get.offAllNamed(Routes.DASHBOARD);
            // });
          }
        } else {
          Get.defaultDialog(
            title: "verifikasi email",
            middleText:
                "silahkahkan verifikasi email dahulu sebelum login, apakah kamu ingin mengirim verifikasi ulang?",
            onConfirm: () async {
              await userlog.user!.sendEmailVerification();
              Get.back();
            },
            textConfirm: "kirim ulang",
            onCancel: () {
              Get.back();
            },
            textCancel: "kembali",
          );
        }
      } on FirebaseAuthException catch (e) {
        if (e.code == 'user-not-found') {
          Get.defaultDialog(
              title: "Akun Tidak ditemukan",
              middleText: "Silahkan melakukan pendaftaran akun",
              onConfirm: () => Get.back(),
              textConfirm: "oke");
        } else if (e.code == 'wrong-password') {
          Get.defaultDialog(
              title: "Password Salah",
              middleText: "Masukkan Password Yang Benar",
              onConfirm: () => Get.back(),
              textConfirm: "oke");
        }
      }
    } else {
      Get.snackbar("Email tidak valid", "masukkan email dengan benar");
    }
  }

  // Logout User

  void logut() async {
    await FirebaseAuth.instance.signOut();
    Get.offAllNamed(Routes.LOGIN_PAGE);
  }
}
  //new chat

  