import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import '../controllers/detail_profil_controller.dart';

class DetailProfilView extends GetView<DetailProfilController> {
  var infoProfil = Get.arguments;
  DetailProfilView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
        title: Text(
          '${infoProfil['nama'].toUpperCase()}',
          style: const TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      body: ListView(
        children: [
          Column(
            children: [
              Container(
                margin: const EdgeInsets.all(15),
                width: 180,
                height: 180,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(200),
                  child: infoProfil!['foto'] != ""
                      ? Image.network(
                          infoProfil['foto'],
                          fit: BoxFit.cover,
                        )
                      : infoProfil!["gender"] == "pria"
                          ? Lottie.asset(
                              "assets/lottie/man.json",
                              fit: BoxFit.cover,
                            )
                          : Lottie.asset(
                              "assets/lottie/woman.json",
                              fit: BoxFit.cover,
                            ),
                ),
              ),
            ],
          ),
          Container(
            decoration: BoxDecoration(
                color: Color.fromARGB(31, 236, 236, 236).withOpacity(0.5),
                borderRadius: BorderRadius.circular(10)),
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Nama",
                        style: const TextStyle(
                          fontSize: 12,
                        )),
                    Text(
                      infoProfil?['nama'],
                      style: const TextStyle(
                          fontSize: 12, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                Divider(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Email",
                        style: const TextStyle(
                          fontSize: 12,
                        )),
                    Text(
                      infoProfil?['email'],
                      style: const TextStyle(
                          fontSize: 12, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                Divider(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Alamat",
                        style: const TextStyle(
                          fontSize: 12,
                        )),
                    Text(
                      infoProfil?['alamat'],
                      style: const TextStyle(
                          fontSize: 12, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                Divider(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("No.Telp/HP",
                        style: const TextStyle(
                          fontSize: 12,
                        )),
                    Text(
                      infoProfil?['noTelp'],
                      style: const TextStyle(
                          fontSize: 12, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
