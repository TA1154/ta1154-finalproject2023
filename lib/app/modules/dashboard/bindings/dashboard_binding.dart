import 'package:get/get.dart';

import '../../Home/controllers/home_controller.dart';
import '../../lapor/controllers/lapor_controller.dart';
import '../../profil/controllers/profil_controller.dart';
import '../controllers/dashboard_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DashboardController>(
      () => DashboardController(),
    );
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<LaporController>(
      () => LaporController(),
    );
    Get.lazyPut<ProfilController>(
      () => ProfilController(),
    );
  }
}
