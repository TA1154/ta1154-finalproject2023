import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:final_project_2023/app/modules/lapor/views/lapor_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../Home/views/home_view.dart';
import '../../profil/views/profil_view.dart';
import '../controllers/dashboard_controller.dart';

class DashboardView extends GetView<DashboardController> {
  const DashboardView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DashboardController>(
      builder: (controller) {
        return Scaffold(
          body: SafeArea(
            child: IndexedStack(
              index: controller.tabindex,
              children: [
                HomeView(),
                LaporView(),
                ProfilView(),
              ],
            ),
          ),
          bottomNavigationBar: ConvexAppBar(
            style: TabStyle.fixedCircle,
            elevation: 5,
            activeColor: Colors.white,
            items: const [
              TabItem(icon: Icons.home, title: 'Beranda'),
              TabItem(icon: Icons.camera, title: 'Lapor'),
              TabItem(icon: Icons.person, title: 'Profil'),
            ],
            initialActiveIndex: controller.tabindex,
            onTap: controller.pindahTabIndex,
          ),
        );
      },
    );
  }
}
