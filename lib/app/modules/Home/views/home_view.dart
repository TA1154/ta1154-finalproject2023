import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import '../../../style/textStyle.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
            top: true,
            left: true,
            right: true,
            child: Container(),
          ),
          StreamBuilder<DocumentSnapshot<Map<String, dynamic>>>(
              stream: controller.streamUser(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                if (snapshot.hasData) {
                  Map<String, dynamic>? dataUser = snapshot.data!.data();
                  print(dataUser);
                  return Container(
                    height: MediaQuery.of(context).size.height / 7,
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      // vertical: 10,
                    ),
                    // margin: const EdgeInsets.only(top: 20),
                    child: Column(
                      children: [
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Hallo, ${dataUser?['nama']}",
                                    style: const TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.w800,
                                      fontSize: 20,
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  const Text(
                                    "Bagaimana kabar Anda hari ini ?",
                                    style: TextStyle(
                                      color: Colors.black38,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                              ClipOval(
                                child: Container(
                                  height: 70,
                                  width: 70,
                                  decoration: BoxDecoration(
                                    // color: Colors.white,
                                    borderRadius: BorderRadius.circular(25),
                                  ),
                                  child: dataUser!['foto'] != ""
                                      ? Image.network(
                                          dataUser['foto'],
                                          fit: BoxFit.cover,
                                        )
                                      : dataUser!["gender"] == "pria"
                                          ? Lottie.asset(
                                              "assets/lottie/man.json",
                                              fit: BoxFit.cover,
                                            )
                                          : Lottie.asset(
                                              "assets/lottie/woman.json",
                                              fit: BoxFit.cover,
                                            ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                } else {
                  return const Center(
                    child: Text("Gagal memuat data."),
                  );
                }
              }),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: Get.height * 0.3,
                width: Get.width / 2,
                child: Lottie.asset("assets/lottie/buang_sampah.json"),
              ),
              Container(
                  width: Get.width / 2.4,
                  // margin: EdgeInsets.only(left: 20, right: 20),
                  child: AnimatedTextKit(
                    animatedTexts: [
                      RotateAnimatedText(
                        'Mengenai Sampah',
                        textStyle: const TextStyle(
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                        // speed: const Duration(milliseconds: 150),
                      ),
                      TypewriterAnimatedText(
                        'Sampah merupakan barang yang sudah tak terpakai atau buangan dari suatu produk dan bisa diolah kembali menjadi barang berguna. Wujudnya bisa berupa padat, cair, dan gas. Sampah sangat erat hubungannya dengan aktivitas manusia di muka bumi. Dalam kehidupan sehari-hari manusia pasti menghasilkan sampah, baik itu dari rumah tangga maupun industri.',
                        textStyle: const TextStyle(
                          fontSize: 12.0,
                        ),
                        speed: const Duration(milliseconds: 100),
                      ),
                    ],
                    isRepeatingAnimation: false,
                    // totalRepeatCount: 4,
                    // pause: const Duration(milliseconds: 1000),
                    displayFullTextOnTap: true,
                    // stopPauseOnTap: true,
                  ))
            ],
          )
        ],
      ),
    ));
  }
}
