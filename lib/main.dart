import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:final_project_2023/app/controllers/auth_c_controller.dart';
import 'app/routes/app_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final authC = Get.put(AuthCController(), permanent: true);
  MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark,
        ),
        child: StreamBuilder<User?>(
          stream: authC.streamStatusAuth,
          builder: (context, snapshot) {
            print(snapshot.data);
            if (snapshot.connectionState == ConnectionState.active) {
              return GetMaterialApp(
                title: "iReport",
                debugShowCheckedModeBanner: false,
                initialRoute: snapshot.data != null &&
                        snapshot.data!.emailVerified == true
                    ? Routes.DASHBOARD
                    : Routes.LOGIN_PAGE,
                getPages: AppPages.routes,
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ));
  }
}
